#include <iostream>

#define ERROR true
#define BAD_VALUE 8200

int add(int a, int b, bool& error) {
    if (a + b == BAD_VALUE) {
        error = ERROR;
    }
  return a + b;
}

int  multiply(int a, int b, bool& error) {
  int sum = 0;
  for(int i = 0; i < b; i++) {
    sum = add(sum, a, error);
  };
  return sum;
}

int  pow(int a, int b, bool& error) {
  int exponent = 1;
  for(int i = 0; i < b; i++) {
    exponent = multiply(exponent, a, error);
  };
  return exponent;
}

void printRes(int res, bool error) {
    if (error) {
        std::cout << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year\n";
    }
    else {
        std::cout << res << std::endl;
    }
}

/*int main(void) {
    bool error = !ERROR;
    int res = 0;

    // add
    std::cout << "add\n";

    error = !ERROR;
    res = add(8199, 1, error);
    printRes(res, error);

    error = !ERROR;
    res = add(8000, 1, error);
    printRes(res, error);

    // mult
    std::cout << "mult\n";

    error = !ERROR;
    res = multiply(4100, 2, error);
    printRes(res, error);

    error = !ERROR;
    res = multiply(4500, 2, error);
    printRes(res, error);

    // pow
    std::cout << "pow\n";
    error = !ERROR;
    res = pow(8200, 1, error);
    printRes(res, error);

    error = !ERROR;
    res = pow(4, 5, error);
    printRes(res, error);

    // special
    std::cout << "error when res != 8200\n";
    error = !ERROR;
    res = multiply(2050, 3, error);
    printRes(res, error);

    error = !ERROR;
    res = multiply(2050, 4, error);
    printRes(res, error);

    error = !ERROR;
    res = multiply(2050, 5, error);
    printRes(res, error);
}*/
#include <iostream>

#define BAD_VALUE 8200

int add(int a, int b) {
    if (a + b == BAD_VALUE) {
        throw std::string("This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year\n");
    }
    return a + b;
}

int  multiply(int a, int b) {
  int sum = 0;
  for(int i = 0; i < b; i++) {
    sum = add(sum, a);
  };
  return sum;
}

int  pow(int a, int b) {
  int exponent = 1;
  for(int i = 0; i < b; i++) {
    exponent = multiply(exponent, a);
  };
  return exponent;
}

int main(void) {
    // add
    std::cout << "add\n";

    try {
        std::cout << add(8199, 1) << std::endl;
    }
    catch (std::string str) {
        std::cout << str;
    }

    try {
        std::cout << add(8000, 1) << std::endl;
    }
    catch (std::string str) {
        std::cout << str;
    }

    // mult
    std::cout << "mult\n";

    try {
        std::cout << multiply(4100, 2) << std::endl;
    }
    catch (std::string str) {
        std::cout << str;
    }

    try {
        std::cout << multiply(4500, 2) << std::endl;
    }
    catch (std::string str) {
        std::cout << str;
    }

    // pow
    std::cout << "pow\n";
    
    try {
        std::cout << pow(8200, 1) << std::endl;
    }
    catch (std::string str) {
        std::cout << str;
    }

    try {
        std::cout << pow(4, 6) << std::endl;
    }
    catch (std::string str) {
        std::cout << str;
    }

    // special
    std::cout << "error when res != 8200\n";
    
    try {
        std::cout << multiply(2050, 3) << std::endl;
    }
    catch (std::string str) {
        std::cout << str;
    }

    try {
        std::cout << multiply(2050, 4) << std::endl;
    }
    catch (std::string str) {
        std::cout << str;
    }

    try {
        std::cout << multiply(2050, 5) << std::endl;
    }
    catch (std::string str) {
        std::cout << str;
    }
}
#include "Hexagon.h"

Hexagon::Hexagon(std::string name, std::string col, double side) : Shape(name, col), _side(side)
{
}

// function displays info of shape
void Hexagon::draw()
{
	std::cout << std::endl << "Color is " << this->getColor() << std::endl << "Name is " << this->getName() << std::endl << "side length is " << this->getSide() << std::endl << "area: " << this->CalArea() << std::endl;;
}
// function returns area of shape
double Hexagon::CalArea()
{
	return MathUtils::CalHexagonArea(this->_side);
}
// getter
double Hexagon::getSide() const
{
	return this->_side;
}
// setter
void Hexagon::setSide(const double value)
{
	if (value < 0) {
		throw shapeException();
	}
	this->_side = value;
}

#pragma once
#include "shape.h"
#include "MathUtils.h"
#include "shapeexception.h"

class Hexagon : public Shape
{
public:
	Hexagon(std::string name, std::string col, double side);
	virtual void draw();
	virtual double CalArea();
	double getSide() const;
	void setSide(const double value);
private:
	double _side;
};


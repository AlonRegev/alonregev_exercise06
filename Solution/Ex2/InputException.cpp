#include "InputException.h"

// ctor - fixes cin
InputException::InputException()
{
	std::cin.clear();
	std::cin.ignore();
}

// function returns description of exception
const char* InputException::what() const
{
	return "Bad Input!";
}

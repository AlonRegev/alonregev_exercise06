#pragma once
#include <exception>
#include <iostream>

class InputException : public std::exception
{
public:
	InputException();

	virtual const char* what() const;
};


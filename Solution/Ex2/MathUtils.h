#pragma once

#define PENTAGON_COEFFICIENT 1.72
#define HEXAGON_COEFFICIENT 2.598

class MathUtils
{
public:
	// function calculate area of a regular polygon based on their side length
	static double CalPentagonArea(const double sideLength) {
		return sideLength * sideLength * PENTAGON_COEFFICIENT;
	}
	static double CalHexagonArea(const double sideLength) {
		return sideLength * sideLength * HEXAGON_COEFFICIENT;
	}
};

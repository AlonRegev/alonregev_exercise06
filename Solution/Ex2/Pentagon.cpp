#include "Pentagon.h"

Pentagon::Pentagon(std::string name, std::string col, double side) : Shape(name, col), _side(side)
{
}

// function displays info of shape
void Pentagon::draw()
{
	std::cout << std::endl << "Color is " << this->getColor() << std::endl << "Name is " << this->getName() << std::endl << "side length is " << this->getSide() << std::endl << "area: " << this->CalArea() << std::endl;;
}
// function returns area of shape
double Pentagon::CalArea()
{
	return MathUtils::CalPentagonArea(this->_side);
}
// getter
double Pentagon::getSide() const
{
	return this->_side;
}
// setter
void Pentagon::setSide(const double value)
{
	if (value < 0) {
		throw shapeException();
	}
	this->_side = value;
}

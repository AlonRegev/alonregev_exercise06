#include <iostream>
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include "Pentagon.h"
#include "Hexagon.h"
#include <string>
#include "shapeException.h"
#include "InputException.h"

int main()
{
	std::string buffer;
	std::string nam, col; double rad = 0, ang = 0, ang2 = 180, side = 0; int height = 0, width = 0;
	Circle circ(col, nam, rad);
	quadrilateral quad(nam, col, width, height);
	rectangle rec(nam, col, width, height);
	parallelogram para(nam, col, width, height, ang, ang2);
	Pentagon pent(nam, col, side);
	Hexagon hex(nam, col, side);


	Shape *ptrcirc = &circ;
	Shape *ptrquad = &quad;
	Shape *ptrrec = &rec;
	Shape *ptrpara = &para;
	Shape* ptrpent = &pent;
	Shape* ptrhex = &hex;


	
	std::cout << "Enter information for your objects" << std::endl;
	const char circle = 'c', quadrilateral = 'q', rectangle = 'r', parallelogram = 'p'; char shapetype;
	char x = 'y';
	while (x != 'x') {
		std::cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram, 5 = pentagon, 6 = hexagon" << std::endl;
		std::cin >> shapetype;
try
		{
	// check if cin is empty 
	std::getline(std::cin, buffer);
	if (!buffer.empty()) {
		throw std::exception("Warning - Don't try to build more than one shape at once ");
	}

			switch (shapetype) {
			case 'c':
				std::cout << "enter color, name,  rad for circle" << std::endl;
				std::cin >> col >> nam >> rad;
				if (std::cin.fail()) {
					throw InputException();
				}
				circ.setColor(col);
				circ.setName(nam);
				circ.setRad(rad);
				ptrcirc->draw();
				break;
			case 'q':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				if (std::cin.fail()) {
					throw InputException();
				}
				quad.setName(nam);
				quad.setColor(col);
				quad.setHeight(height);
				quad.setWidth(width);
				ptrquad->draw();
				break;
			case 'r':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				if (std::cin.fail()) {
					throw InputException();
				}
				rec.setName(nam);
				rec.setColor(col);
				rec.setHeight(height);
				rec.setWidth(width);
				ptrrec->draw();
				break;
			case 'p':
				std::cout << "enter name, color, height, width, 2 angles" << std::endl;
				std::cin >> nam >> col >> height >> width >> ang >> ang2;
				if (std::cin.fail()) {
					throw InputException();
				}
				para.setName(nam);
				para.setColor(col);
				para.setHeight(height);
				para.setWidth(width);
				para.setAngle(ang, ang2);
				ptrpara->draw();
				break;
			case '5':
				std::cout << "enter name, color, side length " << std::endl;
				std::cin >> nam >> col >> side;
				if (std::cin.fail()) {
					throw InputException();
				}
				pent.setName(nam);
				pent.setColor(col);
				pent.setSide(side);
				ptrpent->draw();
				break;
			case '6':
				std::cout << "enter name, color, side length " << std::endl;
				std::cin >> nam >> col >> side;
				if (std::cin.fail()) {
					throw InputException();
				}
				hex.setName(nam);
				hex.setColor(col);
				hex.setSide(side);
				ptrhex->draw();
				break;
			default:
				std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
				break;
			}
			std::cout << "would you like to add more object press any key if not press x" << std::endl;

			std::cin >> x;	// didn't work...
		}
		catch (std::exception& e)	// needed to be a reference. wasn't shapeexception's virtual method because of slicing.
		{	
			printf(e.what());
		}
		catch (...)
		{
			printf("caught a bad exception. continuing as usual\n");
		}
	}



		system("pause");
		return 0;
	
}